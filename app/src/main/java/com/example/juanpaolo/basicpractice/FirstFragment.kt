package com.example.juanpaolo.basicpractice


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_first.*


class FirstFragment : Fragment() {

    var sampleFragmentActivity: SampleFragmentActivity? = null
    var splitFragmentActivity: SplitFragmentActivity? = null

    //ovveride this method to get current activity
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is SampleFragmentActivity) {
            sampleFragmentActivity = context
        } else if(context is SplitFragmentActivity) {
            splitFragmentActivity = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        btnShow.setOnClickListener {
            //show second fragment
            sampleFragmentActivity?.showSecondFragmentWithName(edtName.text.toString())
            splitFragmentActivity?.updateName(edtName.text.toString())

        }
    }


}
