package com.example.juanpaolo.basicpractice

import User.User
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"
    var users = ArrayList<User>()

    val REQ_LOGOUT = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupViews()
    }

    private fun setupViews() {
        btnLogin.setOnClickListener {
            onLoginClick(txtUsername.text.toString(), txtPassword.text.toString())
        }

        btnSignup.setOnClickListener {
            onSignupClick(txtUsername.text.toString(), txtPassword.text.toString())
        }

    }

    fun onLoginClick(username: String, password: String) {
        Log.d(TAG, "Username is $username and password: $password")

        if(isExistingUser({
                   username == it.username && password == it.password
                })) {
            var user = User()
            user.username = username
            user.password = password

            val detailsIntent = Intent(this, DetailsActivity::class.java)
            detailsIntent.putExtra(DetailsActivity.BDL_NAME, username)
            detailsIntent.putExtra(DetailsActivity.BDL_NUM, 69)
            detailsIntent.putExtra(DetailsActivity.BDL_USER, user)
            detailsIntent.putExtra(DetailsActivity.BDL_USER_LIST, users)
            startActivityForResult(detailsIntent, REQ_LOGOUT)

            makeToast("Welcome $username")
        } else {
            makeToast("Account does not exist")
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQ_LOGOUT) {
            if(resultCode == DetailsActivity.RESULT_EMPTY) {
                makeToast("Bukol ka!!!! TANGA MOPOO!!")
            } else if(resultCode == DetailsActivity.RESULT_NOT_EMPTY) {
                val str = data?.getStringExtra(DetailsActivity.BDL_INFO)
                makeToast(str ?: "")
            }
        }
    }

    fun onSignupClick(username: String, password: String) {
        var user = User()
        user.username = username
        user.password = password

        if(isExistingUser( {
                    it.username == username
                })) {
            Log.d(TAG, "$username already exist.")
            makeToast("$username ${resources.getString(R.string.exists)}.")
        } else if (username.isEmpty() && password.isEmpty()) {
            makeToast("Please fill out the fields.")
        } else {
            users.add(user)
            txtUsername.text.clear()
            txtPassword.text.clear()
            makeToast(resources.getString(R.string.registered))
        }

    }

    fun makeToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    fun isExistingUser(checker: (user: User) -> Boolean): Boolean {
        users.forEach {
            if (checker(it)) {
                return true
            }
        }
        return false
    }

}
