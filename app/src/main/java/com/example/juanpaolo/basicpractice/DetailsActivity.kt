package com.example.juanpaolo.basicpractice

import User.User
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {

    companion object {
        val BDL_NAME = "BDL_NAME"
        val BDL_NUM = "BDL_NUM"
        val BDL_USER = "BDL_USER"
        val BDL_USER_LIST = "BDL_USER_LIST"

        val RESULT_NOT_EMPTY = 0
        val RESULT_EMPTY = 1
        val BDL_INFO = "BDL_INFO"
    }

    var name: String? = null
    var number: Int? = null
    var user: User? = null
    var users: ArrayList<User>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        name = intent.getStringExtra(BDL_NAME)
        number = intent.getIntExtra(BDL_NUM, 0)
        user = intent.getParcelableExtra(BDL_USER)

        users = intent.getParcelableArrayListExtra(BDL_USER_LIST)
        txtName.text = "${user?.username} loves number $number"

        var str = ""

        users?.forEach {
            str += "${it.username} ,"
        }

        txtList.text = str

        btnLogout.setOnClickListener {
            val info = edtInfo.text.toString()
            var resultCode: Int = 0
            var data = Intent()
            if(info.isEmpty()) {
                resultCode = RESULT_EMPTY
            } else {
                resultCode = RESULT_NOT_EMPTY
                data.putExtra(BDL_INFO, info)
            }

            setResult(resultCode, data)

            finish()
        }

        cbSomething.setOnCheckedChangeListener { buttonView, isChecked ->
            Toast.makeText(this, "isChecked: $isChecked", Toast.LENGTH_SHORT).show()
        }

        btnCheckRhg.setOnClickListener {
            val selectedId = rgOption.checkedRadioButtonId
            val str = when(selectedId) {
                R.id.rd1 -> "First"
                R.id.rd2 -> "Second"
                R.id.rd3 -> "Third"
                else -> "WALA"
            }
            Toast.makeText(this, "selected: $str", Toast.LENGTH_SHORT).show()
        }
    }
}
