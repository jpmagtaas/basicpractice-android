package com.example.juanpaolo.basicpractice


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_second.*

class SecondFragment : Fragment() {

    companion object {
        val BDL_NAME = "BDL_NAME"

        //function to create a fragment with params
        fun newInstance(name: String) : SecondFragment {
            //create instance of the fragment
            val fragment = SecondFragment()
            //create a Bundle object then assign values
            val bdl = Bundle()
            bdl.putString(BDL_NAME, name)
            //pass other values here...

            //pass the created bundle to your fragment as argument
            fragment.arguments = bdl

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    //call your init inside this method
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        val name = arguments?.getString(BDL_NAME)

        txtName.text = name
    }

    fun changeName(name: String) {
        txtName.text = name
    }


}
