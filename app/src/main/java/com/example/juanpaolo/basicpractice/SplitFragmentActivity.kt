package com.example.juanpaolo.basicpractice

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.example.juanpaolo.basicpractice.R

class SplitFragmentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_split_fragment)

        changeFragment(R.id.container1, FirstFragment())
        changeFragment(R.id.container2, SecondFragment())
    }

    private fun changeFragment(containerId: Int, fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(containerId, fragment).commit()
    }

    fun updateName(name: String) {
        val frag = supportFragmentManager.findFragmentById(R.id.container2)
        if(frag is SecondFragment) {
            frag.changeName(name)
        }
    }
}
