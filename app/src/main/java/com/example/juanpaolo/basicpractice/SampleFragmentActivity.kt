package com.example.juanpaolo.basicpractice

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import kotlinx.android.synthetic.main.activity_sample_fragment.*

class SampleFragmentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample_fragment)

        btnFragment1.setOnClickListener {
            changeFragment(FirstFragment())
        }

        btnFragment2.setOnClickListener {
            changeFragment(SecondFragment.newInstance("Aldrin"))
        }
    }

    private fun changeFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.mainContainer, fragment).commit()
    }

    //Public funciton
    fun showSecondFragmentWithName(name: String) {
        changeFragment(SecondFragment.newInstance(name))
    }
}
