package User

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class User(
        var username: String? = null,
        var password: String? = null
): Parcelable